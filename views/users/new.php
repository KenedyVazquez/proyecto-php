<?php
	include 'toolbar.php';
?>
<form action="./controllers/controller.php?folder=<?= $_GET['folder']; ?>" method="POST">
  <div class="row">
    <div class="col text-center">
      <i class="material-icons" style="font-size: 80px;">add</i>
    </div>
  </div>
  <div class="form-group">
  	 <label for="nombre">Nombres</label>
    <input type="text" class="form-control" id="nombre" name="nombre" autofocus placeholder="Tus nombres" required>
  </div>
  <div class="form-group">
  	 <label for="paterno">Paterno</label>
    <input type="text" class="form-control" id="paterno" name="paterno" placeholder="Tus apellidos" required>
  </div>
  <div class="form-group">
  	 <label for="materno">Materno</label>
    <input type="text" class="form-control" id="materno" name="materno" placeholder="Tus apellidos" required>
  </div>
  <div class="form-group">
  	 <label for="correo">E-mail</label>
    <input type="email" class="form-control" id="correo" name="correo" placeholder="Tu e-mail" required>
  </div>
  <div class="form-group">
  	 <label for="fecha_nacimiento">Facha Nacimiento</label>
    <input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="Tu e-mail" required>
  </div>
  <div class="form-group text-center">
  	<input type="submit" name="create" value="Crear" class="btn btn-primary">
  </div>
  <div class="form-group text-center">
  	<?php
  		if(isset($_GET['success'])){
	?>
			<div class="alert alert-success">
				El usuario ha sido creado.
			</div>
	<?php
  		}else if(isset($_GET['error'])){
  	?>
			<div class="alert alert-danger">
				Ha ocurrido un error al crear el usario, por favor intente de nuevo.
			</div>
	<?php
  		}
  	?>
  </div>
</form>