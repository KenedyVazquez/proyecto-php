<?php

  include './models/users.php';
  $title="Listado de Usuarios";

  $user     = new Users();
  $id       = isset($_GET['id'])?$_GET['id']:null;
  $users    = $user->getUserById($id);
  $nombre     = '';
  $paterno = '';
  $materno = '';
  $correo    = '';
  $fecha_nacimiento = '';
  if($users){
    $nombre     =$users[0]['nombre'];
    $paterno =$users[0]['paterno'];
    $materno =$users[0]['materno'];
    $correo    =$users[0]['correo'];
    $fecha_nacimiento =$users[0]['fecha_nacimiento'];
  }

	include 'toolbar.php';
?>
<form action="./controllers/controller.php?folder=<?= $_GET['folder']; ?>" method="POST">
  <div class="row">
    <div class="col text-center">
      <i class="material-icons" style="font-size: 80px;">edit</i>
    </div>
  </div>
  <div class="form-group">
  	 <label for="nombre">Nombres</label>
    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Tus nombres" autofocus required value="<?php echo $nombre; ?>">
  </div>
  <div class="form-group">
  	 <label for="paterno">Paterno</label>
    <input type="text" class="form-control" id="paterno" name="paterno" placeholder="Tus apellidos" required value="<?php echo $paterno; ?>">
  </div>
  <div class="form-group">
  	 <label for="materno">Materno</label>
    <input type="text" class="form-control" id="materno" name="materno" placeholder="Tus apellidos" required value="<?php echo $materno; ?>">
  </div>
  <div class="form-group">
  	 <label for="correo">E-mail</label>
    <input type="email" class="form-control" id="correo" name="correo" placeholder="Tu e-mail" required value="<?php echo $correo; ?>">
  </div>
  <div class="form-group">
  	 <label for="fecha_nacimiento">E-mail</label>
    <input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="Tu fechanacimiento" required value="<?php echo $fecha_nacimiento; ?>">
  </div>
  <div class="form-group text-center">
  	<input type="submit" name="edit" value="Editar" class="btn btn-primary">
  </div>
  <div class="form-group text-center">
  	<?php
  		if(isset($_GET['success'])){
	?>
			<div class="alert alert-success">
				La informacion ha sido actualizada.
			</div>
	<?php
  		}else if(isset($_GET['error'])){
  	?>
			<div class="alert alert-danger">
				Ha ocurrido un error al editar la información, por favor intente de nuevo.
			</div>
	<?php
  		}
  	?>
  </div>
  <input type="hidden" name="id" value="<?php echo $id ?>">
</form>