-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-01-2021 a las 22:20:13
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestion_empleados_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `det_telefono`
--

CREATE TABLE `det_telefono` (
  `id_telefono` int(11) NOT NULL,
  `telefono` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `det_telefono`
--

INSERT INTO `det_telefono` (`id_telefono`, `telefono`) VALUES
(1, 2111235562),
(2, 2113454323),
(3, 2113454324);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `r_trabajador_det_telefono`
--

CREATE TABLE `r_trabajador_det_telefono` (
  `id_trabajador_det_telefono` int(11) NOT NULL,
  `id_trabajador` int(11) NOT NULL,
  `id_telefono` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `r_trabajador_det_telefono`
--

INSERT INTO `r_trabajador_det_telefono` (`id_trabajador_det_telefono`, `id_trabajador`, `id_telefono`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `trabajador`
--

CREATE TABLE `trabajador` (
  `id_trabajador` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `paterno` varchar(100) NOT NULL,
  `materno` varchar(100) NOT NULL,
  `correo` varchar(150) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `activo` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `trabajador`
--

INSERT INTO `trabajador` (`id_trabajador`, `nombre`, `paterno`, `materno`, `correo`, `fecha_nacimiento`, `activo`) VALUES
(1, 'KenedI', 'Vazquez', 'Mimientzi', 'vazquezkenedy@outlook.com', '2021-01-04', 0),
(2, 'Arely', 'Lopez', 'Guevara', 'guevaraarely@gmail.com', '2020-12-02', 0),
(6, 'salud', 'vazquez', 'buuren', 'adminevents@softura.com.mx', '2021-01-14', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `det_telefono`
--
ALTER TABLE `det_telefono`
  ADD PRIMARY KEY (`id_telefono`);

--
-- Indices de la tabla `r_trabajador_det_telefono`
--
ALTER TABLE `r_trabajador_det_telefono`
  ADD PRIMARY KEY (`id_trabajador_det_telefono`),
  ADD KEY `id_trabajador` (`id_trabajador`),
  ADD KEY `id_det_telefono` (`id_telefono`);

--
-- Indices de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  ADD PRIMARY KEY (`id_trabajador`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `det_telefono`
--
ALTER TABLE `det_telefono`
  MODIFY `id_telefono` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `r_trabajador_det_telefono`
--
ALTER TABLE `r_trabajador_det_telefono`
  MODIFY `id_trabajador_det_telefono` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `trabajador`
--
ALTER TABLE `trabajador`
  MODIFY `id_trabajador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
